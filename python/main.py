import math
import json
import socket
import sys


def calculate_friction_accel(car_obj, curve_radius):
    ''' given a car with its state, and the curve it's on,
        calculate the side friction coefficient '''

    if int(curve_radius) > 0:

        # friction acceleration (constant), can be estimated
        # by the difference in centrifugal acceleration and the acceleration
        # making the car move sideways

        # Friction coefficient varies linearly with speed !
        # friction = m * car_obj.speed
        friction = car_obj.speed*car_obj.speed -\
            car_obj.tail_acceleration

        print "friction: %s, tail_accel: %s" % (friction, car_obj.tail_acceleration)


class Car(object):
    ''' this class describes the state of a car '''

    name = None
    color = None
    length = None
    width = None
    guide = None

    distance = 0
    completed_pieces_distance = 0
    speed = 0  # current speed
    acceleration = 0  # current acceleration

    slip_angle = 0  # current slip angle
    slip_angle_speed = 0  # slip angle speed
    slip_angle_accel = 0   # slip angle acceleration

    tail_radius = 0
    tail_acceleration = 0  # this represents acceleration of
                           # car tail upwards, related to slip
                           # angle acceleration

    def __repr__(self):

        return "Car {0} distance {1} speed {2}, acceleration {3}"\
               " slip {4}, slip speed {5}, tail acc {7}, slip acc {6}".format(
                self.name,
                self.distance,
                self.speed,
                self.acceleration,
                self.slip_angle,
                self.slip_angle_speed,
                self.slip_angle_accel,
                self.tail_acceleration
        )

    def __init__(self, car, dimensions=None):

        self.name = car['name']
        self.color = car['color']

        if dimensions:
            self.width = dimensions['width']
            self.length = dimensions['length']
            self.guide = dimensions['guideFlagPosition']

    def completed_track_piece(self, length):
        # add the length of a completed track piece
        # with the lane car was travelling on

        self.completed_pieces_distance += length

    def update(self, car_dict):
        # "angle": 45.0,
        # "piecePosition": {
        #   "pieceIndex": 0,
        #   "inPieceDistance": 20.0,
        #   "lane": {
        #     "startLaneIndex": 1,
        #     "endLaneIndex": 1
        #   },
        # "lap": 0

        new_distance = self.completed_pieces_distance +\
            car_dict['piecePosition']['inPieceDistance']

        new_speed = new_distance - self.distance

        new_acceleration = new_speed - self.speed

        self.distance = new_distance

        self.speed = new_speed

        self.acceleration = new_acceleration

        new_angle = car_dict['angle']

        new_angle_speed = new_angle - self.slip_angle

        new_angle_accel = new_angle_speed - self.slip_angle_speed

        self.slip_angle = new_angle

        self.slip_angle_speed = new_angle_speed

        self.slip_angle_accel = new_angle_accel

        # this tail acceleration |R * sin(theta)|
        self.tail_radius = abs(self.length - self.guide) * \
            math.sin(math.radians(self.slip_angle))

        self.tail_acceleration = abs(self.length - self.guide) * \
            math.sin(math.radians(self.slip_angle_accel))


class TrackModel(object):
    # describes the track and
    # the position of the car in it

    commands = []

    throttle = 0.5

    opponent_cars = []

    pieces = []

    lanes = []

    last_piece_index = 0

    last_piece_lane_index = 0

    lane_index = 0

    next_curve = None

    current_track_piece = None

    current_track_piece_index = 0

    mycar = None

    switchrequested = False

    def __repr__(self):

        return "TRACK, {0} pieces, lanes {1}, all track pieces {2}".format(
            len(self.pieces), self.lanes, self.pieces)

    def __init__(self, data, mycar):
        ''' initialize track, track pieces, cars, lanes, lane lengths '''

        # get cars
        for car in data['race']['cars']:

            newcar = Car(car['id'], car['dimensions'])

            if car['id']['name'] == mycar.name:

                self.mycar = newcar

            else:
                self.opponent_cars.append(newcar)

        # get lane information
        for lane in data['race']['track']['lanes']:

            self.lanes.append(lane['distanceFromCenter'])

        # get list of track pieces
        for piece in data['race']['track']['pieces']:
            # track piece is a straight line
            if 'length' in piece.keys():

                if 'switch' in piece.keys():

                    self.pieces.append({'length': piece['length'],
                                        'switch': True})

                else:

                    self.pieces.append({'length': piece['length'],
                                        'switch': False})

            # track piece is a curve
            else:

                # list of lengths for each lane in a curve
                lane_lengths = []

                angle = piece['angle']
                radius = piece['radius']

                for lane in self.lanes:

                    # positive angle, lines with negative distance to center
                    # are longer
                    if angle > 0:
                        # invert lane distance
                        lane = -lane

                    else:

                        pass

                    lane_lengths += \
                        [math.pi * abs(angle) / 360 * (2 * (radius + lane))]

                if 'switch' in piece.keys():
                    self.pieces.append({
                        'switch': True,
                        'angle': piece['angle'],
                        'radius': piece['radius'],
                        'length': lane_lengths})

                else:
                    self.pieces.append({
                        'angle': piece['angle'],
                        'radius': piece['radius'],
                        'length': lane_lengths,
                        'switch': False})

    def set_car_positions(self, data, gameTick):
        # set the positions of the cars in this track

        for car in data:

            if car['id']['name'] == self.mycar.name:
                # update my car position

                self.piece_pos = car['piecePosition']

                piece_index = self.piece_pos['pieceIndex']

                self.current_track_piece_index = piece_index

                self.current_track_piece = self.pieces[piece_index]

                self.lane_index = self.piece_pos['lane']['startLaneIndex']

                if (piece_index == 0) and \
                   (self.piece_pos['lap'] == 0):
                    self.last_piece_lane_index = self.lane_index

                elif piece_index != self.last_piece_index:
                # we swtiched piece from last Tick
                    print "SWITCHED TO PIECE %d, %s" % (
                        piece_index,
                        self.pieces[piece_index]
                    )

                    if 'angle' in self.pieces[self.last_piece_index].keys():
                    # previous completed piece was a curve
                        # get the length for the lane where the car is
                        self.mycar.completed_track_piece(
                            self.pieces[
                                self.last_piece_index
                            ]['length'][self.last_piece_lane_index]
                        )

                    else:

                        self.mycar.completed_track_piece(
                            self.pieces[self.last_piece_index]['length']
                        )

                    # update last_piece_index and last_piece_lane_index
                    self.last_piece_index = piece_index

                    self.last_piece_lane_index = self.lane_index

                self.mycar.update(car)

                if 'radius' in self.current_track_piece:

                    calculate_friction_accel(
                        self.mycar, self.current_track_piece['radius']
                    )

                print "at tick {0}, throttle {2}: {1}".format(
                    gameTick, self.mycar, self.throttle)

        return self.send_next_command()

    def send_next_command(self):

        # no switch capability, control throttle
        self.update_throttle()

        return {'throttle': self.throttle}

        next_piece = self.pieces[
            (self.current_track_piece_index+1) % (len(self.pieces))
        ]

        next_curve = self.next_curved_piece()

        if next_piece['switch'] and next_piece != next_curve:

                # they both switch
                if next_curve['switch']:

                    # take wide
                    if next_curve['angle'] < 0:

                        command = {"switchLane": "Right"}

                    elif next_curve['angle'] > 0:

                        command = {"switchLane": "Left"}

                    return command

                else:
                    # only current switches, take short
                    if next_curve['angle'] < 0:

                        command = {"switchLane": "Left"}

                    elif next_curve['angle'] > 0:

                        command = {"switchLane": "Right"}

                    return command

        elif next_piece == next_curve and next_curve['switch']:

            if next_curve['angle'] < 0:

                command = {"switchLane": "Left"}

            elif next_curve['angle'] > 0:

                command = {"switchLane": "Right"}

            return command

        # no switch capability, control throttle
        self.update_throttle()

        return {'throttle': self.throttle}

    def next_curved_piece(self):
        ''' return next piece that is on a curve, not counting current one '''
        next_index = self.current_track_piece_index + 1

        while True:

            if 'angle' in self.pieces[next_index % len(self.pieces)]:

                return self.pieces[next_index % len(self.pieces)]

            next_index += 1

    def update_throttle(self):

        self.throttle = 0.65


class NoobBot(object):

    tick = None
    track = None

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):

        self.send(json.dumps({"msgType": msg_type,
                              "data": data}))

        # slower below

        # self.send(json.dumps({"msgType": msg_type,
        #                      "data": data,
        #                      "gameTick": self.tick}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):

        self.msg("throttle", throttle)

    def switchLane(self, lane):

        self.msg("switchLane", lane)

    def ping(self):

        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        
        next_command = self.track.set_car_positions(data, self.tick)

        if 'throttle' in next_command.keys():

            self.throttle(next_command['throttle'])

        elif 'switchLane' in next_command.keys():
            print "SENDING SWITCH LANE"
            self.switchLane(next_command['switchLane'])

        self.ping()

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_game_init(self, data):
        # sent after on_game_yourcar
        # initialize TrackModel
        self.track = TrackModel(data, self.car)

        self.ping()

    def on_game_yourcar(self, data):
    # initialize Car Model
        self.car = Car(data)
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_lap_finished(self, data):

        if data['car']['name'] == self.car.name:

            print "My car: Lap finished in {0}ticks!".format(data['lapTime']['ticks'])

        self.ping()

    def msg_loop(self):

        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'yourCar': self.on_game_yourcar,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'gameInit': self.on_game_init,
            'error': self.on_error,
            'lapFinished': self.on_lap_finished,
        }
        socket_file = s.makefile()
        line = socket_file.readline()

        while line:
    
            msg = json.loads(line)
    
            msg_type, data = msg['msgType'], msg['data']

            if msg_type == 'carPositions':
                if 'gameTick' in msg.keys():
                    self.tick = msg['gameTick']
                
            if msg_type in msg_map:
                msg_map[msg_type](data)
            
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
